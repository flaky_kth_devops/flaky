// Handles the behaviour of the
let art_mode = false;
let time_of_last_art_mode_toggle = new Date();
let build_failing = false;
let flatline_override = false;
let fetch_interval;

let is_flat = false;
const double_hit = true;

let counter = 0;

const flatlineDuration = 2;
const flatlineRisk = 0.1;

const heartBeatTime = 2;
const timeBetweenBeats = 5-heartBeatTime;

const colourOk = "g";
const colourFlat = "r";

function printEKGToConsole(){
    const dt = 0.001;
    const T = 1;
    for(let t = 0.0; t < T; t += dt){
        console.log(t + ", " + (heartBeat(t, false)).y);
    }
}

function generate_point(dt){
    // A function that decides whether to return a flatline or a heartbeat
    const N = timeBetweenBeats / dt;
    let ret;

    if(flatline_override || is_flat){
        if(counter >= N*flatlineDuration){
            // If the flatline should end, reset it
            is_flat = false;
            counter = 0;
        }
        else{
            counter++;
        }
        // Return a beeping red line
        return {y : 0, sound : true, colour : colourFlat};
    }
    else if(counter === 0){
        // Return the beginning of the heartbeat
        ret = heartBeat(0);
        ret.colour = colourOk;
        counter++;
    }
    else if(counter < N*heartBeatTime){
        // Return the rest of the heartbeat
        ret = heartBeat(counter*dt);
        ret.colour = colourOk;
        counter++;
    }
    else{
        // Inbetween heartbeats return a nice green line
        ret = {y: 0, sound : false, colour: colourOk};
        if(counter >= N){
            // Reset counter and decide if flat
            counter = 0;
            if(Math.random() < flatlineRisk && art_mode){
                is_flat = true;
            }
        }
        else{
            counter++;
        }
    }

    return ret;
}

function heartBeat(t){
    /*
      t is for the inserted time. We will have a discrete jump between 
      time points.
      
      persistent is in case you wish to remember what happened before,
      that is if the flatline should be persistent.

      Returns a tuple of (y, sound) where y is the coordinate and 
      sound is a boolean indicating whether sound should be made or not.
    */

    // These names are not strictly scientific

    // Flat second degree polynomial
    const PInterval = [0, heartBeatTime*0.26];
    // Remain flat
    const PRInterval = [PInterval[1], PInterval[1] + heartBeatTime*0.1];
    // Spiky
    const QRSInterval = [PRInterval[1], PRInterval[1] + heartBeatTime*0.1];
    // Flat
    const STInterval = [QRSInterval[1], QRSInterval[1] + heartBeatTime*0.05];
    // Sine function 1/3 amplitude
    const TInterval = [STInterval[1], STInterval[1] + heartBeatTime*0.48];
    // Sine function 1/10 amplitude
    const UInterval = [TInterval[1], TInterval[1] + heartBeatTime*0.1];

    if(in_interval(t, PInterval)){
        // This looks like a flat topped second degree polynomial
        // Probably make it a sine function with a period 2*PInterval
        // TODO this is not sufficiently flat. Test a 4th degree polynomial instead

        // TODO: Refine this constant, it is the amplitude
        const pHeight = 0.1;
        let T = (t-PInterval[0]);
        return {y : pHeight*Math.sin(T * Math.PI/len_interval(PInterval)), sound : false};
    }
    else if(in_interval(t,QRSInterval)){
        /*
         This looks like it goes down for 1/10th of the interval spikes to 1
         then goes down 1/8th of the interval and then linearly increase to one.
         Will need a lot of gluing.
        */
        let T = t - QRSInterval[0];
        const QRSLength = len_interval(QRSInterval);
        if(T <= 1/4*QRSLength)
            return {y : -2/(5*QRSLength)*(T), sound : true};

        else if(T <= 1/2*QRSLength){
            T += - 1/4*QRSLength;
            return {y : (22/(5*QRSLength) * T - 1/10), sound : true};
        }

        else if(T <= 3/4*QRSLength){
            T += -1/2*QRSLength;
            return{y : (-9/(2*QRSLength) * T + 1), sound : true};
        }

        else{
            T += -3/4 * QRSLength;
            return{y : (T/(2*QRSLength) - 1/8), sound : true};
        }
    }
    else if(in_interval(t,TInterval)){
        const THeight = 1/3;
        let T = t - TInterval[0];
        return {y : THeight*Math.sin(T * Math.PI/len_interval(TInterval)), sound : false};
    }
    else if(in_interval(t, UInterval)){
        const UHeight = 1/20;
        let T = t - UInterval[0];
        return {y : UHeight*Math.sin(T * Math.PI/len_interval(UInterval)), sound : false};
    }
    else if(in_interval(t, PRInterval) || in_interval(t,STInterval)){
        // Beep for the flat parts too
        return {y : 0, sound : false};
    }
    else{
        return {y : 0, sound : false};

    }
}

function in_interval(t, interval){
    // We use left open and right closed intervals
    return t > interval[0] && t <= interval[1];
}

function len_interval(interval){
    return interval[1] - interval[0];
}

const BACKGROUND_COLOR = '#002700';
const BACKGROUND_COLOR_FLAT = '#270000';
const GRID_COLOR = '#00bb00';
const GRID_COLOR_FLAT = '#bb0000';
const GRADIENT_COLOR_OUTER = '#000000';
const GRADIENT_COLOR_INNER = '#00000011';
const GRID_WIDTH = 50;
const GRID_HEIGHT = 50;
const LINE_COLOR_OK = '#00FF00';
const LINE_COLOR_FLAT = '#FF0000';
const UPDATES_PER_SECOND = 100;
const REDRAWS_PER_SECOND = 60;
const MAX_AGE = 5;
const DOT_COLOR = '#FFFFFF';
const MODE_BANNER_TIME = 2000;
const BEEP_TONE = 500;


function resizeCanvasToDisplaySize(canvas) {
    // look up the size the canvas is being displayed
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;

    // If it's resolution does not match change it
    if (canvas.width !== width || canvas.height !== height) {
        canvas.width = width;
        canvas.height = height;
        return true;
    }

    return false;
}

function drawBackground(canvas, context, flatlining) {
    //Draw solid color
    {
        context.fillStyle = flatlining ? BACKGROUND_COLOR_FLAT : BACKGROUND_COLOR;
        context.fillRect(0, 0, canvas.width, canvas.height);
    }

    //Draw grid
    {
        let x_middle = canvas.width/2;
        let y_middle = canvas.height/2;

        context.fillStyle = flatlining ? GRID_COLOR_FLAT : GRID_COLOR;


        for(let dist = 0; y_middle - dist > 0; dist += GRID_HEIGHT) {
            context.fillRect(0, y_middle - dist, canvas.width, 1);
            context.fillRect(0, y_middle + dist, canvas.width, 1);
        }

        for(let dist = 0; x_middle - dist > 0; dist += GRID_WIDTH) {
            context.fillRect(x_middle - dist, 0, 1, canvas.height);
            context.fillRect(x_middle + dist, 0, 1, canvas.height);
        }
    }
}

function ratio_to_hex(percent) {
    let a = Math.floor(percent < 0 ? 0 : percent*255);
    let alpha_in_hex = a.toString(16);
    alpha_in_hex = alpha_in_hex.length < 2 ? `0${alpha_in_hex}` : alpha_in_hex;

    return alpha_in_hex;
}

function drawGradient(canvas, context, x_focus) {
    let x = x_focus;
    let y = canvas.height/2;
    let r = Math.max(canvas.width, canvas.height) * 1.5;

    context.strokeStyle = '#00000000';
    context.beginPath(); context.arc(x, y, r, 0, 2 * Math.PI); context.stroke();

    let grd = context.createRadialGradient(x, y, 0, x, y, r);
    grd.addColorStop(0.0, GRADIENT_COLOR_INNER);
    grd.addColorStop(0.4, GRADIENT_COLOR_OUTER);

    context.fillStyle = grd;
    context.fill();
}

function drawMode(canvas, context) {
    if(new Date().getTime() - time_of_last_art_mode_toggle.getTime() < MODE_BANNER_TIME) {
        context.fillStyle = '#FFFFFF';
        let text = art_mode ? "ART MODE" : "BUILD MODE";
        context.fillText(text, 10, canvas.height - 10);
    }
}

function drawLine(canvas, context, points) {
    let a = points.slice(0, points.length - 1);
    let b = points.slice(1, points.length);
    let lines = a
        .map((e, i) => ({from: e, to: b[i]}))
        .filter((e) => e.from.x < e.to.x);

    for(let line of lines) {
        let x_from = line.from.x;
        let y_from = canvas.height/2 - line.from.y * canvas.height/2 * 0.8;

        let x_to = line.to.x;
        let y_to = canvas.height/2 - line.to.y * canvas.height/2 * 0.8;

        let age_percent = ((line.to.age) / MAX_AGE);
        let alpha = (-0.5*Math.pow(2.3*age_percent-1, 3)+0.5);
        let alpha_in_hex = ratio_to_hex(alpha);
        let base_color = line.to.colour === colourOk ? LINE_COLOR_OK : LINE_COLOR_FLAT;
        let color_with_alpha = base_color.concat(alpha_in_hex);

        context.lineWidth = 4;
        context.strokeStyle = color_with_alpha;
        //context.lineCap = "round";
        context.beginPath();
        context.moveTo(x_from, y_from);
        context.lineTo(x_to, y_to);
        context.stroke();
    }

    let x = points[points.length - 1].x;
    let y = canvas.height/2 - points[points.length - 1].y * canvas.height/2 * 0.8;

    context.fillStyle = DOT_COLOR;
    context.beginPath(); context.arc(x, y, 5,0, 2 * Math.PI); context.fill();
}

function age_points(points) {
    return points
        .map(point => {
            point.age += 1/UPDATES_PER_SECOND;
            return point;
        })
        .filter(point => point.age < MAX_AGE)
}

function toggle_art_mode() {
    art_mode = !art_mode;
    time_of_last_art_mode_toggle = new Date();

    if(art_mode) {
        clearInterval(fetch_interval);
    } else {
        fetch_interval = setInterval(update_status, 30000);
    }
}

function update_status() {
    fetch("https://backend.jesperlarsson.me/status")
        .then(res => res.json())
        .then(fetched_build_failing => build_failing = fetched_build_failing);
}

let mute = true;

function toggle_mute() {
    mute = !mute;

    document.getElementById("mute").innerText = mute ? "🔇" : "🔊";

    if(audioCtx === undefined) {
        audioCtx = new (window.AudioContext || window.webkitAudioContext)();
        audioCtx.suspend();
        oscillator = audioCtx.createOscillator();
        oscillator.type = 'square';
        oscillator.frequency.value = BEEP_TONE;
        oscillator.connect(audioCtx.destination);
        oscillator.start();
    }

    if(mute) {
        audioCtx.suspend();
    }
}

let audioCtx;
let oscillator;
let canvas = document.getElementById('ECG');

if (canvas.getContext) {
    let ctx = canvas.getContext('2d');
    let points = [];
    let x = 0;

    toggle_art_mode();

    setInterval(() => {
        let dt = 1/UPDATES_PER_SECOND;
        let point = generate_point(dt);
        point.age = 0;
        point.x = x;

        if(audioCtx !== undefined) {
            if(!mute && point.sound) {
                audioCtx.resume();
            }

            if(!point.sound) {
                audioCtx.suspend();
            }
        }

        points.push(point);

        points = age_points(points);
        x += canvas.width/UPDATES_PER_SECOND/5;
        x = x % canvas.width;

    }, 1000/UPDATES_PER_SECOND);

    setInterval(() => {
        flatline_override = !art_mode && build_failing;

        resizeCanvasToDisplaySize(canvas);
        drawBackground(canvas, ctx, points[points.length - 1].colour === colourFlat);
        drawGradient(canvas, ctx, canvas.width/2);
        drawLine(canvas, ctx, points);
        drawMode(canvas, ctx);
    }, 1000/REDRAWS_PER_SECOND);
}

